package jettySrv;

import java.util.HashMap;

public class ConfStruct {
	private String context;
	private String mainPkg;
	private HashMap<String,String> servlets;
	
	public String getContext() {
		return context;
	}
	public String getMainPkg() {
		return mainPkg;
	}
	public HashMap<String, String> getServlets() {
		return servlets;
	}
	
}
