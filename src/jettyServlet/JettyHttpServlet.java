package jettyServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jettyLog.Logger;

public class JettyHttpServlet extends HttpServlet {

	private static final long serialVersionUID = -1143480608842658791L;
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp,int status) throws ServletException, IOException{
		Logger.log(req,status);
		super.doGet(req, resp);
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp,int status) throws ServletException, IOException{
		Logger.log(req,status);
		super.doPost(req, resp);
	}
	protected void doPut(HttpServletRequest req, HttpServletResponse resp,int status) throws ServletException, IOException{
		Logger.log(req,status);
		super.doPut(req, resp);
	}
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp,int status) throws ServletException, IOException{
		Logger.log(req,status);
		super.doDelete(req, resp);
	}
	protected void doHead(HttpServletRequest req, HttpServletResponse resp,int status) throws ServletException, IOException{
		Logger.log(req,status);
		super.doHead(req, resp);
	}
}
